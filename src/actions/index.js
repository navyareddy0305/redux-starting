export function BooksData() {
    debugger;
    return {
        type: "BOOKS_DATA",
        data: [{
            key: 1,
            name: "Harry Potter"
        }, {
            key: 2,
            name: "Mistborn"
        }, {
            key: 3,
            name: "Malazan"
        }]
    }
}

export function MoviesData() {
    debugger;
    return {
        type: "MOVIE_DATA",
        data: [{
            key: 1,
            name: "Harry Potter"
        }, {
            key: 2,
            name: "Dark Knight"
        }, {
            key: 3,
            name: "Truman Show"
        }]
    }
}
