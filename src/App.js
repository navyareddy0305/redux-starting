import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { BooksData } from './actions';
import logo from './logo.svg';
import './App.css';

class App extends Component {

  componentDidMount() {
    debugger;
    this.props.booksData();
  }

  render() {
    debugger;
    return (
      <div className="App">
        {this.props.books.map((e) => {
          return <h1>{e.name}</h1>;
        })}
      </div>
    );
  }
}

function mapStateToProps(state) {
  debugger;
  console.log(state);
  return {
    books: state.books
  
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ booksData: BooksData }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
