import {
    
    combineReducers
} from 'redux';
import BooksReducer from "./books";
import MoviesReducer from "./movies";
debugger;

let reducer_data = combineReducers({
    
    books: BooksReducer,
    movies: MoviesReducer
});

export default reducer_data;
